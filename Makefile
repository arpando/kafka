GOPATH ?= $(HOME)/go
GOBIN ?= $(GOPATH)/bin

VERSION = $(shell git describe --tags --always --dirty)
BRANCH = $(shell git rev-parse --abbrev-ref HEAD)

.PHONY: help dep build test clean

all: help

help:
	@echo
	@echo "VERSION: $(VERSION)"
	@echo "BRANCH: $(BRANCH)"
	@echo
	@echo "usage: make <command>"
	@echo
	@echo "commands:"
	@echo "    dep       - populate vendor/ from Gopkg.lock without updating it first"
	@echo "    build     - build apps and installs them in $(GOBIN)"
	@echo "    clean     - remove generated files and directories"
	@echo
	@echo "GOPATH: $(GOPATH)"
	@echo "GOBIN: $(GOBIN)"
	@echo

kafka:
	@echo ">>> Launching kafka..."
	kubectl apply -f kube/zookeeper.yaml
	kubectl apply -f kube/kafka.yaml
	@echo

kafka-shell:
	@echo ">>> Launching kafka shell..."
	kubectl exec -it $(shell kubectl get pods --selector=run=kafka -o=custom-columns=NAME:.metadata.name --no-headers) bash
	@echo

dep:
ifeq (, $(shell which dep))
$(error "Cannot find dep tool, install and make it available in your PATH")
endif
	@echo ">>> Ensuring dependencies..."
	@dep ensure

build:
	@echo ">>> Building app..."
	go install -v ./...
	@echo

clean:
	@echo ">>> Cleaning..."
	go clean -i -r -cache -testcache
	@echo

