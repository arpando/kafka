package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/Shopify/sarama"
)

const (
	kafkaConn = "localhost:9092"
	topic     = "words"
)

type SyncProducer struct {
	sarama.SyncProducer
}

type ProducerConfig struct {
	ClientID      string
	MaxRetries    int
	RequiredAcks  int16
	ReturnSuccess bool
}

type ProducerResponse struct {
	Partition int32
	Offset    int64
	Err       error
}

func NewSyncProducer(addrs []string, cfg *ProducerConfig) (*SyncProducer, error) {
	config := sarama.NewConfig()
	config.ClientID = cfg.ClientID
	config.Producer.Retry.Max = cfg.MaxRetries
	config.Producer.RequiredAcks = sarama.RequiredAcks(cfg.RequiredAcks)
	config.Producer.Return.Successes = cfg.ReturnSuccess

	prod, err := sarama.NewSyncProducer(addrs, config)
	if err != nil {
		return nil, err
	}

	return &SyncProducer{prod}, nil
}

func (p *SyncProducer) Publish(topic string, message []byte) ProducerResponse {
	partition, offset, err := p.SendMessage(
		&sarama.ProducerMessage{
			Topic: topic,
			Value: sarama.ByteEncoder(message),
		},
	)
	return ProducerResponse{partition, offset, err}
}

func main() {
	// create producer
	producer, err := NewSyncProducer([]string{kafkaConn}, &ProducerConfig{
		ClientID:      "words-producer",
		MaxRetries:    5,
		RequiredAcks:  -1,
		ReturnSuccess: true,
	})

	sarama.Logger = log.New(os.Stdout, "", log.Ltime)

	if err != nil {
		fmt.Println("Error producer: ", err.Error())
		os.Exit(1)
	}

	var (
		partition int32
		offset    int64
		reader    = bufio.NewReader(os.Stdin)
	)

	for {
		msg, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		resp := producer.Publish(topic, []byte(msg))
		if resp.Err != nil {
			fmt.Println("Error publish: ", resp.Err.Error())
			break
		}
		partition, offset = resp.Partition, resp.Offset
	}
	fmt.Println("Partition: ", partition)
	fmt.Println("Offset: ", offset)
}
