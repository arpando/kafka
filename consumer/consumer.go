package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/Shopify/sarama"
	"github.com/wvanbergen/kafka/consumergroup"
)

const (
	zookeeperConn = "localhost:2181"
	cgroup        = "words_group"
	topic         = "words"
)

func main() {
    os.Exit(realMain())
}

func realMain() (ret int) {
	var (
		err error
		cg  *consumergroup.ConsumerGroup
	)

	defer func() {
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			ret = 1
		}
	}() // consumer config

	sarama.Logger = log.New(os.Stdout, "", log.Ltime)

	config := consumergroup.NewConfig()
	config.Offsets.Initial = sarama.OffsetOldest
	config.Offsets.ProcessingTimeout = 10 * time.Second

	// join to consumer group
	cg, err = consumergroup.JoinConsumerGroup(cgroup, []string{topic}, []string{zookeeperConn}, config)
	if err != nil {
		return
	}

	defer cg.Close()

	// run consumer
	for {
		select {
		case msg := <-cg.Messages():
			// messages coming through chanel
			// only take messages from subscribed topic
			if msg.Topic != topic {
				continue
			}

			fmt.Println("Topic: ", msg.Topic)
			fmt.Println("Value: ", string(msg.Value))

			// commit to zookeeper that message is read
			// this prevent read message multiple times after restart
			err := cg.CommitUpto(msg)
			if err != nil {
				fmt.Println("Error commit zookeeper: ", err.Error())
			}
		}
	}
}
