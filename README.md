## Kafka basic producer and consumer

### Launch kafka
```
make kafka
```

### Run producer and post some data to kafka
```
cat words | go run producer/producer.go
```

### Run consumer
```
go run consumer/consumer.go
```

### Log into kafka container
```
make kafka-shell
# list topics
kafka-topics.sh --list --zookeeper $KAFKA_ZOOKEEPER_CONNECT
```
